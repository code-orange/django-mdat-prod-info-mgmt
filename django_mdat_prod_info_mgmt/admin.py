from django.apps import apps
from django.contrib import admin

for model in apps.get_app_config("django_mdat_prod_info_mgmt").models.values():
    admin.site.register(model)
