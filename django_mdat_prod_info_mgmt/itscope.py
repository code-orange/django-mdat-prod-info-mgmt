import time

import requests
from django.conf import settings
from requests.auth import HTTPBasicAuth


def itscope_search_item(params: dict):
    param_list = list()

    for param, value in params.items():
        param_list.append(param + "=" + str(value))

    request_url = "https://api.itscope.com/2.1/products/search/{}/standard.json".format(
        ";".join(param_list)
    )

    session = requests.Session()
    session.headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
    }

    # create arguments for the request
    request_args = {
        "url": request_url,
        "params": {
            "item": 1,
        },
        "auth": HTTPBasicAuth(
            settings.ITSCOPE_API_AUTH_USER, settings.ITSCOPE_API_AUTH_PASSWD
        ),
    }

    # workaround rate limit
    time.sleep(0.5)

    # do the request
    response = session.get(**request_args)

    if response.status_code not in (200, 201, 204):
        return False

    if response.status_code == 204:
        return True

    api_data = response.json()

    return api_data


def itscope_fetch_item(itscope_id: int):
    request_url = "https://api.itscope.com/2.1/products/id/{}/standard.json".format(
        str(itscope_id)
    )

    session = requests.Session()
    session.headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
    }

    # create arguments for the request
    request_args = {
        "url": request_url,
        "auth": HTTPBasicAuth(
            settings.ITSCOPE_API_AUTH_USER, settings.ITSCOPE_API_AUTH_PASSWD
        ),
    }

    # workaround rate limit
    time.sleep(0.5)

    # do the request
    response = session.get(**request_args)

    if response.status_code not in (200, 201, 204):
        return False

    if response.status_code == 204:
        return True

    api_data = response.json()

    return api_data


def itscope_fetch_product_export(export_code: str):
    request_url = "https://api.itscope.com/2.1/products/exports/{}".format(export_code)

    session = requests.Session()
    session.headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
    }

    # create arguments for the request
    request_args = {
        "url": request_url,
        "auth": HTTPBasicAuth(
            settings.ITSCOPE_API_AUTH_USER, settings.ITSCOPE_API_AUTH_PASSWD
        ),
    }

    # workaround rate limit
    time.sleep(0.5)

    # do the request
    response = session.get(**request_args)

    if response.status_code not in (200, 201, 204):
        return False

    if response.status_code == 204:
        return True

    api_data = response.content

    return api_data


def itscope_fetch_categories():
    request_url = "https://api.itscope.com/2.1/products/producttypes/producttype.json"

    session = requests.Session()
    session.headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
    }

    # create arguments for the request
    request_args = {
        "url": request_url,
        "auth": HTTPBasicAuth(
            settings.ITSCOPE_API_AUTH_USER, settings.ITSCOPE_API_AUTH_PASSWD
        ),
    }

    # workaround rate limit
    time.sleep(0.5)

    # do the request
    response = session.get(**request_args)

    if response.status_code not in (200, 201, 204):
        return False

    if response.status_code == 204:
        return True

    api_data = response.json()

    return api_data


def itscope_fetch_manufacturers():
    request_url = "https://api.itscope.com/2.1/company/manufacturer/company.json"

    session = requests.Session()
    session.headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
    }

    # create arguments for the request
    request_args = {
        "url": request_url,
        "auth": HTTPBasicAuth(
            settings.ITSCOPE_API_AUTH_USER, settings.ITSCOPE_API_AUTH_PASSWD
        ),
    }

    # workaround rate limit
    time.sleep(0.5)

    # do the request
    response = session.get(**request_args)

    if response.status_code not in (200, 201, 204):
        return False

    if response.status_code == 204:
        return True

    api_data = response.json()

    return api_data
