import csv

from django.core.management.base import BaseCommand

from django.conf import settings

from django_mdat_customer.django_mdat_customer.models import MdatItems
from django_mdat_prod_info_mgmt.django_mdat_prod_info_mgmt.models import (
    MdatItemPictures,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        with open("pictures.csv") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=";", quotechar='"')

            for row in csv_reader:
                print("ARTNR " + row[0] + " PIC: " + row[1])

                try:
                    pic_file = open("Z:\\" + row[1], "rb")
                except FileNotFoundError:
                    continue

                # import pictures
                new_pic = MdatItemPictures(
                    name=row[1],
                    item=MdatItems.objects.get(
                        external_id=row[0], created_by_id=settings.MDAT_ROOT_CUSTOMER_ID
                    ),
                )

                new_pic.picture.save(row[1], pic_file)

                new_pic.save()

                pic_file.close()
