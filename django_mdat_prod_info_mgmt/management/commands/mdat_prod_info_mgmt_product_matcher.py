import os
import re
from time import sleep
from urllib.parse import quote

import requests
from django.conf import settings
from django.core.management.base import BaseCommand

from django_mdat_customer.django_mdat_customer.models import MdatItems


class Command(BaseCommand):
    @staticmethod
    def RepresentsInt(s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    def handle(self, *args, **options):
        while True:
            os.system("cls" if os.name == "nt" else "clear")

            self.stdout.write(
                " _____               _            _     __  __       _       _               "
            )
            self.stdout.write(
                "|  __ \             | |          | |   |  \/  |     | |     | |              "
            )
            self.stdout.write(
                "| |__) | __ ___   __| |_   _  ___| |_  | \  / | __ _| |_ ___| |__   ___ _ __ "
            )
            self.stdout.write(
                "|  ___/ '__/ _ \ / _` | | | |/ __| __| | |\/| |/ _` | __/ __| '_ \ / _ \ '__|"
            )
            self.stdout.write(
                "| |   | | | (_) | (_| | |_| | (__| |_  | |  | | (_| | || (__| | | |  __/ |   "
            )
            self.stdout.write(
                "|_|   |_|  \___/ \__,_|\__,_|\___|\__| |_|  |_|\__,_|\__\___|_| |_|\___|_|   "
            )
            self.stdout.write(
                "============================================================================="
            )

            try:
                item = (
                    MdatItems.objects.filter(itscope_id__isnull=True)
                    .filter(itscope_skip=False)
                    .filter(created_by_id=settings.MDAT_ROOT_CUSTOMER_ID)
                    .order_by("external_id")
                    .first()
                )
            except MdatItems.DoesNotExist:
                break

            if item is None:
                break

            self.stdout.write("ITEM ID: " + str(item.id))
            self.stdout.write("ITEM CODE: " + item.external_id)
            self.stdout.write("ITEM NAME: " + item.name)

            normalized_name = re.sub("[^a-zA-Z0-9-_*.]", " ", item.name)

            self.stdout.write("ITEM NAME normalized: " + normalized_name)

            keywords = str(quote(normalized_name, safe=""))
            api_url = (
                "https://api.itscope.com/2.1/products/search/keywords="
                + keywords
                + "/standard.json?realtime=false&plzproducts=false&page=1&item=0&sort=DEFAULT"
            )

            self.stdout.write("API-URL: " + api_url)

            self.stdout.write(
                "============================================================================="
            )

            # workaround for rate filter
            sleep(0.5)

            response = requests.get(
                api_url,
                auth=(settings.ITSCOPE_API_AUTH_USER, settings.ITSCOPE_API_AUTH_PASSWD),
            )

            if response.status_code == 404:
                item.itscope_skip = True
                item.save()
                continue

            response_data = response.json()

            if response.status_code == 200:
                result_count = 20

                if len(response_data["product"]) < result_count:
                    result_count = len(response_data["product"])

                print("MATCHES:")

                for i in range(result_count):
                    self.stdout.write("---")

                    if "price" not in response_data["product"][i]:
                        response_data["product"][i]["price"] = 0

                    if "stock" not in response_data["product"][i]:
                        response_data["product"][i]["stock"] = 0

                    if "manufacturerSKU" not in response_data["product"][i]:
                        response_data["product"][i]["manufacturerSKU"] = 0

                    itscope_name = str()
                    itscope_name += str(response_data["product"][i]["manufacturerSKU"])
                    itscope_name += " - "
                    itscope_name += str(response_data["product"][i]["manufacturerName"])
                    itscope_name += " - "
                    itscope_name += str(response_data["product"][i]["productName"])

                    self.stdout.write(str(i) + " - Title: " + itscope_name)

                print("")
                user_choice = input("Is this a match? (s)kip, [0-9]: ")

                if self.RepresentsInt(user_choice):
                    choice_int = int(user_choice)

                    if choice_int >= result_count:
                        continue

                    item.itscope_id = response_data["product"][choice_int]["puid"]
                    item.save()

                if user_choice == "s":
                    item.itscope_skip = True
                    item.save()
