# Generated by Django 3.1.14 on 2022-01-07 22:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_customer", "0015_add_index_for_external_id"),
        ("django_mdat_prod_info_mgmt", "0006_add_mdat_item_itscope_eximports"),
    ]

    operations = [
        migrations.CreateModel(
            name="MdatItemDataSheetData",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("bytes", models.BinaryField()),
                ("filename", models.CharField(max_length=255)),
                ("mimetype", models.CharField(max_length=50)),
            ],
            options={
                "db_table": "mdat_item_datasheet_data",
            },
        ),
        migrations.CreateModel(
            name="MdatItemDataSheets",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=255)),
                (
                    "datasheet",
                    models.FileField(
                        blank=True,
                        null=True,
                        upload_to="django_mdat_prod_info_mgmt.MdatItemDataSheetData/bytes/filename/mimetype",
                    ),
                ),
                (
                    "item",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="django_mdat_customer.mdatitems",
                    ),
                ),
            ],
            options={
                "db_table": "mdat_item_datasheets",
            },
        ),
    ]
