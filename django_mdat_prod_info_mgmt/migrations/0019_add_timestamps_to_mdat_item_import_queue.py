# Generated by Django 3.2.13 on 2022-04-18 20:20

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        (
            "django_mdat_prod_info_mgmt",
            "0018_add_timestamps_to_mdat_item_barcode_types",
        ),
    ]

    operations = [
        migrations.AddField(
            model_name="mdatitemimportqueue",
            name="date_added",
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
    ]
