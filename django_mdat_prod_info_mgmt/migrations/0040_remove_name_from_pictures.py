# Generated by Django 3.2.13 on 2022-04-26 12:43

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_prod_info_mgmt", "0039_remove_name_from_datasheets"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="mdatitempictures",
            name="name",
        ),
    ]
