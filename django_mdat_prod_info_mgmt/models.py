from datetime import datetime
from io import BytesIO

from PIL import Image
from dirtyfields import DirtyFieldsMixin
from django.db import models
from jsonfield import JSONField
from parler.models import TranslatableModel, TranslatedFields

from django_mdat_customer.django_mdat_customer.models import MdatItems


class MdatItemPictures(DirtyFieldsMixin, models.Model):
    item = models.ForeignKey(MdatItems, models.DO_NOTHING)
    content = models.BinaryField()
    date_added = models.DateTimeField(default=datetime.now)
    date_changed = models.DateTimeField(default=datetime.now)

    def save(self, *args, **kwargs):
        image = Image.open(BytesIO(self.content))
        image_file = BytesIO()
        image.save(image_file, format="WEBP")

        self.content = image_file.getvalue()

        if self.is_dirty():
            self.date_changed = datetime.now()

        super(MdatItemPictures, self).save(*args, **kwargs)

    def data(self, img_format: str = "WEBP"):
        image = Image.open(BytesIO(self.content))
        image_file = BytesIO()

        # convert RGBA to RGB for JPEG
        if img_format == "JPEG" and image.mode == "RGBA":
            pixel_data = image.load()

            # If the image has an alpha channel, convert it to white
            # Otherwise we'll get weird pixels
            for y in range(image.size[1]):  # For each row ...
                for x in range(image.size[0]):  # Iterate through each column ...
                    # Check if it's opaque
                    if pixel_data[x, y][3] < 255:
                        # Replace the pixel data with the colour white
                        pixel_data[x, y] = (255, 255, 255, 255)
            image = image.convert("RGB")

        image.save(image_file, format=img_format)

        return image_file.getvalue()

    def resize(self, img_format: str = "WEBP", size_x: int = 200, size_y: int = 200):
        image = Image.open(BytesIO(self.data(img_format=img_format)))
        image_file = BytesIO()

        image.thumbnail((size_x, size_y), Image.LANCZOS)
        image.save(image_file, format=img_format)

        return image_file.getvalue()

    class Meta:
        db_table = "mdat_item_pictures"


class MdatItemDataSheets(DirtyFieldsMixin, models.Model):
    item = models.ForeignKey(MdatItems, models.DO_NOTHING)
    content = models.BinaryField()
    date_added = models.DateTimeField(default=datetime.now)
    date_changed = models.DateTimeField(default=datetime.now)

    def save(self, *args, **kwargs):
        if self.is_dirty():
            self.date_changed = datetime.now()

        super(MdatItemDataSheets, self).save(*args, **kwargs)

    class Meta:
        db_table = "mdat_item_datasheets"


class MdatItemBarcodes(models.Model):
    barcode = models.CharField(max_length=255)
    item = models.ForeignKey(MdatItems, models.DO_NOTHING)
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        unique_together = (("barcode", "item"),)
        db_table = "mdat_item_barcodes"


class MdatItemBarcodeTypes(models.Model):
    name = models.CharField(max_length=255, unique=True)
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "mdat_item_barcode_types"


class MdatItemImportQueue(models.Model):
    code = models.CharField(max_length=255)
    code_type = models.ForeignKey(MdatItemBarcodeTypes, models.DO_NOTHING)
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        unique_together = (("code", "code_type"),)
        db_table = "mdat_item_import_queue"


class MdatItemItscopeExImports(models.Model):
    export_code = models.CharField(max_length=255, unique=True)
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "mdat_item_itscope_eximports"


class MdatItemCategory(DirtyFieldsMixin, TranslatableModel):
    parent_category = models.ForeignKey("self", models.DO_NOTHING, default=1)
    itscope_id = models.CharField(max_length=255, unique=True)
    date_added = models.DateTimeField(default=datetime.now)
    date_changed = models.DateTimeField(default=datetime.now)

    translations = TranslatedFields(
        name=models.TextField(),
        needs_translation=models.BooleanField(default=True),
    )

    def save(self, *args, **kwargs):
        if self.is_dirty():
            self.date_changed = datetime.now()

        super(MdatItemCategory, self).save(*args, **kwargs)

    class Meta:
        db_table = "mdat_item_category"


class MdatItemCategoryToItem(DirtyFieldsMixin, models.Model):
    item = models.OneToOneField(MdatItems, models.DO_NOTHING, primary_key=True)
    category = models.ForeignKey(MdatItemCategory, models.DO_NOTHING, default=1)
    date_added = models.DateTimeField(default=datetime.now)
    date_changed = models.DateTimeField(default=datetime.now)

    def save(self, *args, **kwargs):
        if self.is_dirty():
            self.date_changed = datetime.now()

        super(MdatItemCategoryToItem, self).save(*args, **kwargs)

    class Meta:
        db_table = "mdat_item_category_to_item"


class MdatItemManufacturer(DirtyFieldsMixin, models.Model):
    name = models.CharField(max_length=250)
    itscope_id = models.CharField(max_length=255, unique=True)
    date_added = models.DateTimeField(default=datetime.now)
    date_changed = models.DateTimeField(default=datetime.now)
    company_logo = models.BinaryField(editable=True, null=True, blank=True)

    def save(self, *args, **kwargs):
        image = Image.open(BytesIO(self.company_logo))
        image_file = BytesIO()
        image.save(image_file, format="WEBP")

        self.company_logo = image_file.getvalue()

        if self.is_dirty():
            self.date_changed = datetime.now()

        super(MdatItemManufacturer, self).save(*args, **kwargs)

    def company_logo_data(self, img_format: str = "WEBP"):
        image = Image.open(BytesIO(self.company_logo))
        image_file = BytesIO()

        # convert RGBA to RGB for JPEG
        if img_format == "JPEG" and image.mode == "RGBA":
            pixel_data = image.load()

            # If the image has an alpha channel, convert it to white
            # Otherwise we'll get weird pixels
            for y in range(image.size[1]):  # For each row ...
                for x in range(image.size[0]):  # Iterate through each column ...
                    # Check if it's opaque
                    if pixel_data[x, y][3] < 255:
                        # Replace the pixel data with the colour white
                        pixel_data[x, y] = (255, 255, 255, 255)
            image = image.convert("RGB")

        image.save(image_file, format=img_format)

        return image_file.getvalue()

    def company_logo_resize(
        self, img_format: str = "WEBP", size_x: int = 200, size_y: int = 200
    ):
        image = Image.open(BytesIO(self.company_logo_data(img_format=img_format)))
        image_file = BytesIO()

        image.thumbnail((size_x, size_y), Image.LANCZOS)
        image.save(image_file, format=img_format)

        return image_file.getvalue()

    class Meta:
        db_table = "mdat_item_manufacturer"


class MdatItemManufacturerToItem(DirtyFieldsMixin, models.Model):
    item = models.OneToOneField(MdatItems, models.DO_NOTHING, primary_key=True)
    manufacturer = models.ForeignKey(MdatItemManufacturer, models.DO_NOTHING, default=1)
    date_added = models.DateTimeField(default=datetime.now)
    date_changed = models.DateTimeField(default=datetime.now)

    def save(self, *args, **kwargs):
        if self.is_dirty():
            self.date_changed = datetime.now()

        super(MdatItemManufacturerToItem, self).save(*args, **kwargs)

    class Meta:
        db_table = "mdat_item_manufacturer_to_item"


class MdatItemCacheItscope(DirtyFieldsMixin, models.Model):
    item = models.OneToOneField(MdatItems, models.DO_NOTHING, primary_key=True)
    content = JSONField()
    date_added = models.DateTimeField(default=datetime.now)
    date_changed = models.DateTimeField(default=datetime.now)

    def save(self, *args, **kwargs):
        if self.is_dirty():
            self.date_changed = datetime.now()

        super(MdatItemCacheItscope, self).save(*args, **kwargs)

    class Meta:
        db_table = "mdat_item_cache_itscope"


class MdatItemCacheAlso(DirtyFieldsMixin, models.Model):
    item = models.OneToOneField(MdatItems, models.DO_NOTHING, primary_key=True)
    content = JSONField()
    date_added = models.DateTimeField(default=datetime.now)
    date_changed = models.DateTimeField(default=datetime.now)

    def save(self, *args, **kwargs):
        if self.is_dirty():
            self.date_changed = datetime.now()

        super(MdatItemCacheAlso, self).save(*args, **kwargs)

    class Meta:
        db_table = "mdat_item_cache_also"
