from django.urls import path
from .views import mdat_item_picture

urlpatterns = [
    path(
        "mdat-item-picture/<int:mdat_item_id>_<int:size_x>x<int:size_y>.<str:extension>",
        mdat_item_picture,
    ),
    path("mdat-item-picture/<int:mdat_item_id>.<str:extension>", mdat_item_picture),
]
