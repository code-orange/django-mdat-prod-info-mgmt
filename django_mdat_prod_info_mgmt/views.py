from django.http import HttpResponse, HttpResponseNotFound

from django_mdat_prod_info_mgmt.django_mdat_prod_info_mgmt.models import (
    MdatItemPictures,
)


def mdat_item_picture(
    request, mdat_item_id: int, extension: str, size_x: int = None, size_y: int = None
):
    for picture in MdatItemPictures.objects.filter(item__id=mdat_item_id):
        if size_x and size_y:
            data = picture.resize(extension, size_x, size_y)
        else:
            data = picture.data(extension)

        return HttpResponse(data, content_type="application/octet-stream")

    return HttpResponseNotFound
